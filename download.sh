#! /bin/sh

if [ -f "chromedriver" ]
then
	echo "Driver already downloaded"
else
	unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)     MACHINE=linux64;;
	    Darwin*)    MACHINE=mac64;;
	    CYGWIN*)    MACHINE=linux64;;
	    MINGW*)     MACHINE=linux64;;
	    *)          MACHINE="UNKNOWN:${unameOut}"
	esac
	echo "Downloading driver for ${MACHINE}"
	curl -o chromedriver.zip http://chromedriver.storage.googleapis.com/2.36/chromedriver_$MACHINE.zip
	unzip chromedriver.zip
	rm chromedriver.zip
	echo "Downloading standalone selenium"
	curl -o selenium-server-standalone-3.11.0.jar https://selenium-release.storage.googleapis.com/3.11/selenium-server-standalone-3.11.0.jar
fi
