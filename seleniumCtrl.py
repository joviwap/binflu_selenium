import platform
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

SELENIUM_PATH = '../binflu_selenium/chromedriver'

def connect(remoteAddress=False):
    if(not remoteAddress):
        print("opening local selenium driver")
        driver = webdriver.Chrome(getLocalPath(), chrome_options=getOptions())
    else:
        print("connecting to remote selenium driver on address {}".format(remoteAddress))
        driver = webdriver.Remote(
           remoteAddress,
           getOptions().to_capabilities())
    return driver


def getLocalPath():
    ext = '.exe' if (platform.system() == "Windows") else ''
    return SELENIUM_PATH + ext

def getOptions():
    options = webdriver.ChromeOptions()
    # options.add_argument('headless')
    options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})
    return options
